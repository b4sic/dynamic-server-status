<?php include 'config/config.php';?>

<!DOCTYPE html>
<html lang="de">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Minecraft</title>

	<link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet" />

	<style>
	html,
	body {
		font-family: Nunito, sans-serif;
		width: 100%;
		height: 100vh;
		padding: 0;
		margin: 0;
		background: #eee;
	}

	body {
		display: flex;
		justify-content: center;
		align-items: center;
		background-image: url("assets/background.jpg");
		background-repeat: no-repeat;
		background-size: cover;
	}

	.inner {
		background: #fff;
		padding: 40px 80px;
		text-align: center;
	}

	.addressField {
		font-size: 6vw;
		font-weight: bold;
		width: 100%;
	}

	.statusField {
		font-size: 2vw;
		width: 100%;
		color: #777;
	}
	</style>
</head>

<body>
	<div class="inner">
		<div class="addressField">
			<?php echo getAddress(); ?>
		</div>
		<div class="statusField">
			Status: <?php echo getStatus(); ?>
		</div>
	</div>
</body>

</html>