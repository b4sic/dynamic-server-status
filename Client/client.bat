@echo off

rem webserver: your own webserver with domain or static ip
set webserver="http://localhost"

rem configPassword: your configured password
set configPassword="kXP5bbdp5Qzkudd4D9gBKt7quQWPH4a4"

rem delay=120 : 2 minutes delay
set delay=120


:sendRequest

echo.
echo ###########################
echo Sending request...
echo.
echo Response:
powershell (Invoke-WebRequest -Uri %webserver% -Method Post -Body @{pwd = '%configPassword%'}).Content
echo.
echo Request sent.
echo ###########################
timeout /t %delay% /nobreak

goto sendRequest