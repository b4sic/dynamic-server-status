<?php

$readPassword = "8kDxXj7QOdrQnXfT";
$configPassword = "kXP5bbdp5Qzkudd4D9gBKt7quQWPH4a4";
$configFile = $_SERVER['DOCUMENT_ROOT'] . "/config/config.json";
$allowedDelay = 300; // -> 5 minutes

$clientParameter = "pwd";
$serverParameter = "pwd";

$config = [];

function createConfigFile()
{
    global $configFile;
    global $config;

    $config['address'] = "";
    $config['timestamp'] = date_timestamp_get(date_create());

    file_put_contents(
        $configFile,
        json_encode($config)
    );
}

function loadConfig()
{
    global $configFile;
    global $config;

    if (file_exists($configFile)) {
        $configData = file_get_contents($configFile);

        if ($configData) {

            $config = json_decode($configData, true);

            if (!$config || empty($config)) {
                createConfigFile();
            }

        } else {
            createConfigFile();
        }
    } else {
        createConfigFile();
    }
}

function checkServerUpdate()
{
    global $configFile;
    global $config;
    global $configPassword;
    global $serverParameter;

    if (isset($_SERVER['REMOTE_ADDR']) && !empty($_POST) && isset($_POST[$serverParameter]) && $_POST[$serverParameter] == $configPassword) {

        $config['address'] = $_SERVER['REMOTE_ADDR'];
        $config['timestamp'] = date_timestamp_get(date_create());

        file_put_contents(
            $configFile,
            json_encode($config)
        );

        die("Config updated.");
    }
}

function checkPassword()
{
    global $readPassword;
    global $clientParameter;

    if (!empty($_GET) && isset($_GET[$clientParameter]) && ($_GET[$clientParameter] == $readPassword)) {
        return true;
    } else {
        return false;
    }
}

function getAddress()
{
    global $config;

    if (checkPassword() && $config['address'] != "") {
        return $config['address'];
    } else {
        return "Error";
    }
}

function getStatus()
{
    global $config;
    global $allowedDelay;

    if (checkPassword()) {

        $currentTimestamp = date_timestamp_get(date_create());
        $delay = $currentTimestamp - $config['timestamp'];

        if ($delay < $allowedDelay && $config['address'] != "") {
            return "Available";
        } else {
            return "Unavailable";
        }
    } else {
        return "Not allowed";
    }
}

loadConfig();
checkServerUpdate();