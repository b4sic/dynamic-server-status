#!/usr/bin/env bash

# webserver: your own webserver with domain or static ip
webserver="http://localhost"

# configPassword: your configured password
configPassword="kXP5bbdp5Qzkudd4D9gBKt7quQWPH4a4"

# delay=2m : 2 minutes delay
delay="2m"

while true; do

	echo ""
	echo "###########################"
	echo "Sending request..."
	echo ""
	echo $(curl --data "pwd=$configPassword" $webserver)
	echo ""
	echo "Request sent."
	echo "###########################"
	
	echo ""
	echo "Waiting $delay."
	sleep $delay

done