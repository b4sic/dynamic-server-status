<h1>Dynamic Server Status</h1>

This is a server status updater for servers with dynamic ip addresses.  
A simple webserver with a domain or a static ip address with php support is currently required.

How to request your webserver to get the game servers ip:
**[url]?pwd=[readPassword]**

Please edit the config in Server\config\config.php before uploading to your webserver.
- $readPassword -> the password for your players to see the ip address
- $configPassword -> the password for your game server (->client), to change the ip address.

Change the configPassword variable for your game server (->client) as well.